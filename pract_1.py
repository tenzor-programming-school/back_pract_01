import random


# сумма цифр
def digits_sum(num):
    even_sum = 0 # сумма четных чисел
    odd_sum = 0 # сумма нечетных чисел
    for digit in str(num):
        if int(digit) % 2 == 0:
            even_sum += int(digit)
        else:
            odd_sum += int(digit)
    return odd_sum, even_sum

# лесенка
def draw_stairs(size):
    for i in range(size):
        print(" " * (size - i - 1) + "#" * (i + 1))

# угадай число
def guess_number():
    print("Загадайте число от 1 до 100.")
    min_number = 1
    max_number = 100
    
    while True:
        guess = random.randint(min_number, max_number)
        print(f"Моё предположение: {guess}")
        
        feedback = input("1. 'Больше'\n2. 'Меньше'\n3. 'Верно'\nВыбери нужный вариант: ").lower()
        
        if feedback == "3":
            print("Программа угадала число!")
            break
        elif feedback == "1":
            min_number = guess + 1
        elif feedback == "2":
            max_number = guess - 1
        else:
            print("Некорректный ввод. Попробуйте еще раз.")


def main():
    while True:
        print("Выберите задачу:")
        print("1. Сумма цифр")
        print("2. Лесенка")
        print("3. Угадай число")
        choice = input("Введите номер задачи или 'exit' для выхода: ")

        if choice.lower() == "exit":
            print("Программа завершена.")
            break
        elif choice == "1":
            number = int(input("Введите целое число: "))
            odd_sum, even_sum = digits_sum(number)
            print("Сумма нечетных и четных цифр:", odd_sum, even_sum)
        elif choice == "2":
            size = int(input("Введите размер лесенки (от 1 до 20): "))
            if 1 <= size <= 20:
                draw_stairs(size)
            else:
                print("Ошибка: размер лесенки должен быть от 1 до 20.")
        elif choice == "3":
            guess_number()
        else:
            print("Некорректный выбор, попробуйте еще раз.")

if __name__ == "__main__":
    main()